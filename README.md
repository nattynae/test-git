## git checkout
- git checkout -b for create branch
## git status
- git status for checking file changes
## git branch
- git branch -a for list all remote and local branch
## git push
- git push for uploading local to repo (-f for force push)
## git pull
- git pull for update local branch (also use for merging)
## git add
- git add for adding changes to stage change
## git commit
- git commit -m "message" for creating commit in local
## git merge
- git merge for merge source branch into current branch
## git reset
- git reset for moving index of branch to specific commit(with SHA) 
## git rebase
- git rebase for changing initial code base commit
## git cherry-pick
- git cherry-pick for grabbing commit into current branch